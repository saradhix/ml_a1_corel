clear
%load('feat1normdist.mat');
load('feat2normdist.mat');
corel_path = '/home/saradhix/corel5kvecs/';
trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);


%code to remove frequent labels more/less
frequencies = sum(trainAnnotations);

for i=1:size(trainAnnotations,2)
  counts(i,1)=i;
    counts(i,2)=frequencies(i)
    end

    sorted=sortrows(counts,-2);
    percent_to_remove = 75;

    num_labels_to_remove = percent_to_remove*260/100;
    labels_to_be_removed = [];
    for i=1:num_labels_to_remove
      labels_to_be_removed(end +1 ) =sorted(i);
      end

      labels_to_be_removed=sort(labels_to_be_removed)

      labels_to_be_removed
      num_labels_to_remove
      for i=num_labels_to_remove:-1:1
        trainAnnotations(:,labels_to_be_removed(i)) = [];
          testAnnotations(:,labels_to_be_removed(i)) = [];
          end

%perf=twopassknn(dist,trainAnnotations,testAnnotations,5,10,5);
perf=twopassknn(norm_distances,trainAnnotations,testAnnotations,5,10,5);
perf
