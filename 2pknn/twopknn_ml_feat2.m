clear

randSeed = 1;
randn('state' , randSeed);
rand('state' , randSeed);

metric='euclidean';
corel_path = '/home/saradhix/corel5kvecs/';
load trainFeatures.mat;
load randpick.txt
train = zeros(4500,258);
for i=1:4500
  train(randpick(i),:) = projTrainFtrs(i,:);
end
m = size(train, 1);
f =2;
distances=zeros(m,m,f);
for i=1:m
  for j=1:m
    for k=1:f
      distances = mydistance(train(i,k),train(j,k),metric);
    end
  end
end

for k=1:f
  mn = min(min(distances(i,j,:)));
  mx = max(max(distances(i,j,:)));
%  if mx==mn
%    continue;
%  endif
  distances(i,j,:) = (distances(i,j,:) -  mn) / (mx - mn);
end


trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);
w=ones(f,1);
p = optMetricW(w, distances, trainAnnotations, testAnnotations, 5, '/tmp/model.out', 2);
p

