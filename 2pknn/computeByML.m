clear

corel_path = '/home/vijay/Documents/PG/monsoon2015/ML/Assignment1 ML 2k15/data/corel5k.20091111/';
trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);
trainAnnot=double(trainAnnotations);
testAnnot=double(testAnnotations);
dist=optDistance(trainAnnot);
dist_norm=featureNormalize(dist);

weight_op='metricl.mat';
W=[];
W=optMetricW(W,dist_norm,trainAnnot,5,weight_op,1,15);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load trainFeatures.mat;
load randpick.txt
train = zeros(4500,258);
for i=1:4500
  train(randpick(i),:) = projTrainFtrs(i,:);
end
load testFeatures.mat;
test = projTestFtrs;

train_w=zeros(size(train));
test_w=zeros(size(test));
for g=1:size(W,1)
    train_w(:,g)=train(:,g).*W(g);
    test_w(:,g)=test(:,g).*W(g);
end
metric='euclidean';
pairwise_distances = mydistance(train_w, test_w, metric);
norm_distances = featureNormalize(pairwise_distances);

perf=twopassknn(norm_distances,trainAnnotations,testAnnotations,5,5,5);
save('pnn_ml.mat','perf');