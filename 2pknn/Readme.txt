This is an updated version that includes  
- a more efficient code for learning 'v' metric, and  
- a code for learning 'w' metric once 'v' metric is learned. 

The folder contains the codes for:
- 2PKNN method
- metric-learning (w and v)
- annotation performance evaluation

For scalability, the codes for metric-learning are implemented in 
a manner similar to the implementation of the Pegasos algorithm, 
and can be run on subsets of data. Thus, familiarity with Pegasos 
would be useful in understanding the metric-leanring codes. 

To learn a combined metric (w+v), the user would require to learn 
them using the corresponding codes in an alternate manner.

Please kindly cite our ECCV 2012 paper if you find any of these 
codes useful. 
Also, please send a mail to "yashaswi.verma@research.iiit.ac.in" 
for any query/feedback/bug.



