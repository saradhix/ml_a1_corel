function [W] = optMetricW(W,distanceViVj,trainAnnotations,K,modelFileName,initCount,numOfPasses)

% W = initial weight vector, with sum of all entries being equal to 
%     the number of features considered. E.g. a vector of all 1's
%
% distanceViVj = mxmxf matrix; where m is number of training images, 
%                and f is number of features considered. (i,j,k)^th entry 
%                corresponds to the distance between i^th and jth images 
%                using k^th feature, appropriatly normalized between [0,1].
%
% trainAnnotations = mxn binary matrix with entries in {0,1}; where 
%                    n is the number of labels in vocabulary.
%
% K = number of nearest-neighbours considered in the first-pass 
%     of 2pknn (e.g., 1 to 5)
%
% modelFileName = File path for saving the 'W' vector
%
% initCount = intial count used to initialize learning rate in 
%             the gradient-descent process (e.g., 1)
%
% numOfPasses = number of passes over mxm pairs


randSeed = 1;
randn('state',randSeed);
rand('state',randSeed);

numOfTrainImages = size(trainAnnotations,1);
numOfLabels = size(trainAnnotations,2);
featuresTaken = size(distanceViVj,3);


if( sum(W)>0 )
	W = W/sum(W);
	W = W*featuresTaken;
else
	W = zeros(featuresTaken,1) + 1;
end;


lambdapr = trainAnnotations*trainAnnotations';
%lambdapr=matProduct(trainAnnotations,trainAnnotations');
sumAnnot = sum(trainAnnotations');
for p = 1:numOfTrainImages
	for q = 1:numOfTrainImages
		if( sumAnnot(q)>0 )
			lambdapr(p,q) = lambdapr(p,q)/sumAnnot(q);
		end;
	end;
end;


avgDistanceViVj = zeros(numOfTrainImages,numOfTrainImages);
for i = 1:featuresTaken
	avgDistanceViVj = avgDistanceViVj + W(i)*distanceViVj(:,:,i);
end;


posSampleTable = {numOfLabels};
for i = 1:numOfLabels
	posSampleTable{i} = (find(trainAnnotations(:,i)==1))';
end;
posLabelTable = {numOfTrainImages};
negLabelTable = {numOfTrainImages};
for i = 1:numOfTrainImages
	posLabelTable{i} = find(trainAnnotations(i,:)==1);
	negLabelTable{i} = find(trainAnnotations(i,:)==0);
end;


mue = 1;
count = initCount;
part12 = zeros(length(W),1);
part2 = zeros(length(W),1);
updateW = zeros(length(W),1);

disp('Start.');
for iteration = 1:numOfPasses
	tic;
	permutation = randperm(numOfTrainImages);
	disp(['- ' num2str(count) ' -']);
	for i = permutation
	
		count = count + 1;
		learningRate = 1/(count);

		currPosLabels = posLabelTable{i};
		tarIndx = zeros(10^5,1);
		tarCount = 0;
		for j = currPosLabels
			tempNbrs = posSampleTable{j};
			tempNbrs = tempNbrs(tempNbrs~=i);
			if( length(tempNbrs>0) )
				pickK = min(K,length(tempNbrs));
				tempNbrsDist = avgDistanceViVj(i,tempNbrs)';
				for k = 1:pickK
					[currVal,currIndx] = min(tempNbrsDist);
					tempNbrsDist(currIndx) = inf;
					tarCount = tarCount + 1;
					tarIndx(tarCount) = tempNbrs(currIndx);
				end;
			end;
		end;
		tarIndx = unique(tarIndx(1:tarCount));
		tarCount = length(tarIndx);

		tempUpdateW = zeros(featuresTaken,1);
		if( tarCount>0 )
			currNegLabels = negLabelTable{i};
			impIndx = zeros(10^5,1);
			impCount = 0;
			for j = currNegLabels
				tempNbrs = posSampleTable{j};
				pickK = min(K,length(tempNbrs));
				tempNbrsDist = avgDistanceViVj(i,tempNbrs)';
				for k = 1:pickK
					[currVal,currIndx] = min(tempNbrsDist);
					tempNbrsDist(currIndx) = inf;
					impCount = impCount + 1;
					impIndx(impCount) = tempNbrs(currIndx);
				end;
			end;
			impIndx = unique(impIndx(1:impCount));

			
			for j = 1:tarCount 
				pickTarIndx = tarIndx(j);

				part12(:) = distanceViVj(i,pickTarIndx,:);
				for k = 1:length(impIndx)
					currLoss = 1 + avgDistanceViVj(i,pickTarIndx) - avgDistanceViVj(i,impIndx(k));
					if( currLoss>0 )
						part2(:) = distanceViVj(i,impIndx(k),:);
						tempUpdateW = tempUpdateW + mue*(1-lambdapr(i,impIndx(k)))*(part12-part2);
					end;
				end;
				tempUpdateW = tempUpdateW + part12;
			end;

			updateW = updateW + learningRate*tempUpdateW;

		end;
	end;

	W = W - updateW;
	W(W<=0) = 10^(-30); 
	W = W/sum(W);
	W = W*featuresTaken;

	updateW = zeros(featuresTaken,1);

	avgDistanceViVj = avgDistanceViVj*0; 
	for j = 1:featuresTaken
		avgDistanceViVj = avgDistanceViVj + W(j)*distanceViVj(:,:,j);
	end;
	
	save(modelFileName,'W');

	toc;

end;



