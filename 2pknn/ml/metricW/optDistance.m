function [ dist ] = optDistance( X )
%OPTDISTANCE Summary of this function goes here
%   Detailed explanation goes here
m=size(X,1);
n=size(X,1);
f=size(X,2);
dist=[];
for c=1:n
	p=X(c,:);
	for d=1:m
		q=X(d,:);
        for e=1:f
            t = (p(e)-q(e))*(p(e)-q(e));
            dist(d,c,e)=t;
        end
	end
end

end

