clear

randSeed = 1;
randn('state' , randSeed);
rand('state' , randSeed);

corel_path = '/home/saradhix/corel5kvecs/';
feats1 = struct('feats', {{'DenseHue', 'cityblock'}, {'DenseHueV3H1', 'cityblock'}, {'DenseSift', 'chisq'}, {'DenseSiftV3H1', 'chisq'}, {'Gist', 'euclidean'}, {'HarrisHue', 'cityblock'}, {'HarrisHueV3H1' 'cityblock'}, {'HarrisSift', 'chisq'}, {'HarrisSiftV3H1', 'chisq'}, {'Hsv', 'cityblock'}, {'HsvV3H1', 'cityblock'}, {'Lab', 'kld'}, {'LabV3H1', 'kld'}, {'Rgb', 'cityblock'}, {'RgbV3H1', 'cityblock'}});
pd_flag = 1;


for i=1:15
fd = feats1(i).feats;
feat = cell2mat(fd(1));
metric = cell2mat(fd(2));
train = dir(strcat(corel_path, 'corel5k_train_', feat, '.*vec*'));
%test = dir(strcat(corel_path, 'corel5k_test_', feat, '.*vec*'));
disp('feature being loaded :')
disp(train.name);
train = vec_read([corel_path train.name]);
%test = vec_read([ corel_path test.name]);
if pd_flag
m = size(train, 1);
%n = size(test, 1);
pairwise_distances=zeros(m,m,15);
pd_flag = 0;
end
temp = mydistance(train, train, metric);
temp(isnan(temp))=0;
mn = min(min(temp));
mx = max(max(temp));
norm_distances = (temp -  mn) / (mx - mn);
pairwise_distances(:,:,i) = norm_distances;
end

trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);
trainAnnot=double(trainAnnotations);
testAnnot=double(testAnnotations);
weight_op='metricl.mat';
W=[];
W=optMetricW(W,pairwise_distances,trainAnnot,5,weight_op,1,15);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



for g=1:size(W,1)
    pairwise_distance=pairwise_distances+(pairwise_distances(:,:,g).*(W(g)));
end
pairwise_distance = pairwise_distance ./ 15;
mn = min(min(pairwise_distance));
mx = max(max(pairwise_distance));
norm_distances = (pairwise_distance -  mn) / (mx - mn);

perf=twopassknn(norm_distances,trainAnnotations,testAnnotations,5,5,5);
save('pnn_ml.mat','perf');
