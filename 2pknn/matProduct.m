function theMatrixProduct = matProduct(x,y )
%MATPRODUCT Summary of this function goes here
%   Detailed explanation goes here
[rowsx, colsx] = size(x);
[rowsy, colsy] = size(y);
theMatrixProduct = zeros(rowsx, colsy);
for row = 1 : rowsx
	row % Print progress to command window.
	for col = 1 : colsy
		theSum = 0;
		for k = 1 : colsx
			theSum = theSum + x(row, k) * y(k, col);
		end
		theMatrixProduct(row, col) = theSum;
	end
end

end

