clear

randSeed = 1;
randn('state' , randSeed);
rand('state' , randSeed);

corel_path = '/home/saradhix/corel5kvecs/';

load trainFeatures.mat;
load randpick.txt
train = zeros(4500,258);
for i=1:4500
train(randpick(i),:) = projTrainFtrs(i,:);
end
load testFeatures.mat;
test = projTestFtrs
m = size(train, 1);
n = size(test, 1);
pairwise_distances = zeros(m, n);
metric='euclidean';
pairwise_distances = mydistance(train, test, metric);

mn = min(min(pairwise_distances));
mx = max(max(pairwise_distances));
norm_distances = (pairwise_distances -  mn) / (mx - mn);



trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);

sum(sum(trainAnnotations))
percent_to_remove=60
num_labels_remove=round(percent_to_remove*sum(sum(trainAnnotations))/100)
num_labels_remove
while num_labels_remove > 0
  index = ceil(rand()*4500*260);
  if(trainAnnotations(index)==1)
    trainAnnotations(index)=0;
    num_labels_remove = num_labels_remove - 1;
  end;
end

sum(sum(trainAnnotations))


  p = JEC(norm_distances, trainAnnotations, testAnnotations, 5);
  p
  percent_to_remove
