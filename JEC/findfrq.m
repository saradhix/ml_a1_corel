clear

randSeed = 1;
randn('state' , randSeed);
rand('state' , randSeed);

corel_path = '/home/saradhix/corel5kvecs/';
feats1 = struct('feats', {{'DenseHue', 'cityblock'}, {'DenseHueV3H1', 'cityblock'}, {'DenseSift', 'chisq'}, {'DenseSiftV3H1', 'chisq'}, {'Gist', 'euclidean'}, {'HarrisHue', 'cityblock'}, {'HarrisHueV3H1' 'cityblock'}, {'HarrisSift', 'chisq'}, {'HarrisSiftV3H1', 'chisq'}, {'Hsv', 'cityblock'}, {'HsvV3H1', 'cityblock'}, {'Lab', 'kld'}, {'LabV3H1', 'kld'}, {'Rgb', 'cityblock'}, {'RgbV3H1', 'cityblock'}});
pd_flag = 1;

for i=1:15
fd = feats1(i).feats;
feat = cell2mat(fd(1));
metric = cell2mat(fd(2));
train = dir(strcat(corel_path, 'corel5k_train_', feat, '.*vec*'));
test = dir(strcat(corel_path, 'corel5k_test_', feat, '.*vec*'));
disp(train.name);
train = vec_read([corel_path train.name]);
test = vec_read([ corel_path test.name]);
size(train)
size(test)
if pd_flag
m = size(train, 1);
n = size(test, 1);
pairwise_distances = zeros(m, n);
pd_flag = 0;
end
temp = mydistance(train, test, metric);
temp(isnan(temp))=0;
mn = min(min(temp));
mx = max(max(temp));
norm_distances = (temp -  mn) / (mx - mn);
pairwise_distances = pairwise_distances + norm_distances;
end

pairwise_distances = pairwise_distances ./ 15;
mn = min(min(pairwise_distances));
mx = max(max(pairwise_distances));
norm_distances = (pairwise_distances -  mn) / (mx - mn);


trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);

frequencies = sum(trainAnnotations);

for i=1:size(trainAnnotations,2)
  counts(i,1)=i;
  counts(i,2)=frequencies(i)
end

sorted=sortrows(counts,2);
percent_to_remove = 75;

num_labels_to_remove = percent_to_remove*260/100;
labels_to_be_removed = [];
for i=1:num_labels_to_remove
  labels_to_be_removed(end +1 ) =sorted(i);
end

labels_to_be_removed=sort(labels_to_be_removed)

labels_to_be_removed
num_labels_to_remove
for i=num_labels_to_remove:-1:1
  trainAnnotations(:,labels_to_be_removed(i)) = [];
  testAnnotations(:,labels_to_be_removed(i)) = [];
end

size(trainAnnotations)
size(testAnnotations)



p = JEC(norm_distances, trainAnnotations, testAnnotations, 5);
p
percent_to_remove
