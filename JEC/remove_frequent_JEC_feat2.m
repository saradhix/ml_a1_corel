clear

randSeed = 1;
randn('state' , randSeed);
rand('state' , randSeed);

corel_path = '/home/saradhix/corel5kvecs/';

load trainFeatures.mat;
load randpick.txt
train = zeros(4500,258);
for i=1:4500
train(randpick(i),:) = projTrainFtrs(i,:);
end
load testFeatures.mat;
test = projTestFtrs
m = size(train, 1);
n = size(test, 1);
pairwise_distances = zeros(m, n);
metric='euclidean';
pairwise_distances = mydistance(train, test, metric);

mn = min(min(pairwise_distances));
mx = max(max(pairwise_distances));
norm_distances = (pairwise_distances -  mn) / (mx - mn);



trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);

frequencies = sum(trainAnnotations);

for i=1:size(trainAnnotations,2)
  counts(i,1)=i;
counts(i,2)=frequencies(i)
  end

  sorted=sortrows(counts,2);%To sort in descend give -
  percent_to_remove = 75;

  num_labels_to_remove = percent_to_remove*260/100;
  labels_to_be_removed = [];
  for i=1:num_labels_to_remove
  labels_to_be_removed(end +1 ) =sorted(i);
  end

labels_to_be_removed=sort(labels_to_be_removed)

  labels_to_be_removed
  num_labels_to_remove
  for i=num_labels_to_remove:-1:1
  trainAnnotations(:,labels_to_be_removed(i)) = [];
  testAnnotations(:,labels_to_be_removed(i)) = [];
  end

size(trainAnnotations)
size(testAnnotations)



  p = JEC(norm_distances, trainAnnotations, testAnnotations, 5);
  p
  percent_to_remove
