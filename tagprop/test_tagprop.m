
clear

mex tagpropCmt.c

load corel5k.mat
corel_path = '/home/saradhix/corel5kvecs/';
[ model ll ] = tagprop_learn(NN,[],Y);

P = tagprop_predict(NN,[],model);
P
testAnnotations = vec_read([corel_path 'corel5k_test_annot.hvecs']);
testAnnotations = double(testAnnotations);
perf = computePerf(testAnnotations, P, 5);
size(testAnnotations)
size(P)
perf

