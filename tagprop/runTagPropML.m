clear

mex tagpropCmt.c

corel_path = '/home/saradhix/corel5kvecs/';
trainAnnotations = vec_read([corel_path 'corel5k_train_annot.hvecs']);
trainAnnotations = double(trainAnnotations);

feats1 = struct('feats', {{'DenseHue', 'cityblock'}, {'DenseHueV3H1', 'cityblock'}, {'DenseSift', 'chisq'}, {'DenseSiftV3H1', 'chisq'}, {'Gist', 'euclidean'}, {'HarrisHue', 'cityblock'}, {'HarrisHueV3H1' 'cityblock'}, {'HarrisSift', 'chisq'}, {'HarrisSiftV3H1', 'chisq'}, {'Hsv', 'cityblock'}, {'HsvV3H1', 'cityblock'}, {'Lab', 'kld'}, {'LabV3H1', 'kld'}, {'Rgb', 'cityblock'}, {'RgbV3H1', 'cityblock'}});
pd_flag = 1;

for i=1:15
    fd = feats1(i).feats;
    feat = cell2mat(fd(1));
    metric = cell2mat(fd(2));
    train = dir(strcat(corel_path, 'corel5k_train_', feat, '.*vec*'));
    train = strcat(corel_path,train.name);
    train = vec_read(train);
    if pd_flag
        m = size(train, 1);
        NN = zeros(m, m);
        %DNN = zeros(15, 200, m);
        pd_flag = 0;
    end
    if strcmp(metric, 'chisq')
        %pd = chisq(train, train);
        pd = pdist2(train, train, 'cityblock');
    elseif strcmp(metric, 'kld')
        pd = pdist2(train, train, 'cityblock');
        %pd = chisq(train, train);
        %temp = KLD(train, train);
    else
        pd = pdist2(train, train, metric);
    end
    pd = normalize(pd);
    NN = NN+pd;
    %dnn= sort(pd);
   % dnn = dnn(2:201, :);
   % DNN(i, :, :) = dnn;
end
NN = NN ./15;
NN = normalize(NN);
[DNN, NN] = sort(NN);
clear dummy pd dnn

NN = NN(2:201, :);
DNN = DNN(2:201, :);
DNN=reshape(DNN,1,size(DNN,1),size(DNN,2));
[ model, ll ] = tagprop_learn(NN, DNN, trainAnnotations', 'type', 'dist', 'sigmoids', false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pd_flag = 1;
for i=1:15
    fd = feats1(i).feats;
    feat = cell2mat(fd(1));
    metric = cell2mat(fd(2));
    train = dir(strcat(corel_path, 'corel5k_train_', feat, '.*vec*'));
    train = strcat(corel_path,train.name);
    test = dir(strcat(corel_path, 'corel5k_test_', feat, '.*vec*'));
    test = strcat(corel_path,test.name);
    train = vec_read(train);
    test = vec_read(test);
    if pd_flag
        m = size(train, 1);
        n = size(test, 1);
        NN = zeros(m, n);
       % DNN = zeros(15, 200, n);
        pd_flag = 0;
    end
    if strcmp(metric, 'chisq')
        %pd = chisq(train, test);
        pd = pdist2(train, test, 'cityblock');
    elseif strcmp(metric, 'kld')
        %pd = chisq(train, test);
        %temp = KLD(train, test);
        pd = pdist2(train, test, 'cityblock');
    else
        pd = pdist2(train, test, metric);
    end
    %pd(isnan(pd))=0;
    pd = normalize(pd);
    NN = NN+pd;
    if any(isnan(pd))
        disp([i,'Nan'])
    end
%     dnn= sort(pd);
%     dnn = dnn(1:200, :);
%     DNN(i, :, :) = dnn;
end

NN = NN ./15;
NN = normalize(NN);
[DNN, NN] = sort(NN);
clear dummy pd dnn

NN = NN(2:201, :);
DNN = DNN(2:201, :);
DNN=reshape(DNN,1,size(DNN,1),size(DNN,2));
P = tagprop_predict(NN, DNN, model);

%{
m = size(P, 1); n = size(P, 2);
for i=1:n
    sortedPi = sort(P(i, :));
    P(i, :) = P(i, :) > sortedPi(1, n-5);
end
%}
P = P';

testAnnotations = vec_read([ corel_path 'corel5k_test_annot.hvecs']);
testAnnotations = double(testAnnotations);

perf = computePerf(testAnnotations', P, 5);
perf
