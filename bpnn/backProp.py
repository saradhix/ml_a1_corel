# Back-Propagation Neural Networks
# 
# Written in Python.  See http://www.python.org/
# Placed in the public domain.
# Neil Schemenauer <nas@arctrix.com>

import math
import random
import string
import numpy
random.seed(0)

# calculate a random number where:  a <= rand < b
def rand(a, b):
    return (b-a)*random.random() + a

# Make a matrix (we could use NumPy to speed this up)
def makeMatrix(I, J, fill=0.0):
    m = []
    for i in range(I):
        m.append([fill]*J)
    return m

# our sigmoid function, tanh is a little nicer than the standard 1/(1+e^-x)
def sigmoid(x):
    return math.tanh(x)

# derivative of our sigmoid function, in terms of the output (i.e. y)
def dsigmoid(y):
    return 1.0 - y**2

class NN:
    def __init__(self, ni, nh, no):
        # number of input, hidden, and output nodes
        self.ni = ni + 1 # +1 for bias node
        self.nh = nh
        self.no = no

        # activations for nodes
        self.ai = [1.0]*self.ni
        self.ah = [1.0]*self.nh
        self.ao = [1.0]*self.no
        
        # create weights
        self.wi = makeMatrix(self.ni, self.nh)
        self.wo = makeMatrix(self.nh, self.no)
        # set them to random vaules
        for i in range(self.ni):
            for j in range(self.nh):
                self.wi[i][j] = rand(-0.2, 0.2)
        for j in range(self.nh):
            for k in range(self.no):
                self.wo[j][k] = rand(-2.0, 2.0)

        # last change in weights for momentum   
        self.ci = makeMatrix(self.ni, self.nh)
        self.co = makeMatrix(self.nh, self.no)

    def update(self, inputs):
        if len(inputs) != self.ni-1:
            raise ValueError('wrong number of inputs')

        # input activations
        for i in range(self.ni-1):
            #self.ai[i] = sigmoid(inputs[i])
            self.ai[i] = inputs[i]

        # hidden activations
        for j in range(self.nh):
            sum = 0.0
            for i in range(self.ni):
                sum = sum + self.ai[i] * self.wi[i][j]
            self.ah[j] = sigmoid(sum)

        # output activations
        for k in range(self.no):
            sum = 0.0
            for j in range(self.nh):
                sum = sum + self.ah[j] * self.wo[j][k]
            self.ao[k] = sigmoid(sum)

        return self.ao[:]


    def backPropagate(self, targets, N, M):
        if len(targets) != self.no:
            raise ValueError('wrong number of target values')

        # calculate error terms for output
        output_deltas = [0.0] * self.no
        for k in range(self.no):
            error = targets[k]-self.ao[k]
            output_deltas[k] = dsigmoid(self.ao[k]) * error

        # calculate error terms for hidden
        hidden_deltas = [0.0] * self.nh
        for j in range(self.nh):
            error = 0.0
            for k in range(self.no):
                error = error + output_deltas[k]*self.wo[j][k]
            hidden_deltas[j] = dsigmoid(self.ah[j]) * error

        # update output weights
        for j in range(self.nh):
            for k in range(self.no):
                change = output_deltas[k]*self.ah[j]
                self.wo[j][k] = self.wo[j][k] + N*change + M*self.co[j][k]
                self.co[j][k] = change
                #print N*change, M*self.co[j][k]

        # update input weights
        for i in range(self.ni):
            for j in range(self.nh):
                change = hidden_deltas[j]*self.ai[i]
                self.wi[i][j] = self.wi[i][j] + N*change + M*self.ci[i][j]
                self.ci[i][j] = change

        # calculate error
        error = 0.0
        for k in range(len(targets)):
            error = error + 0.5*(targets[k]-self.ao[k])**2
        return error


    def test(self, patterns):
        result = []
        for p in patterns:
             result.append(self.update(p[0]))
        return result

    def weights(self):
        print('Input weights:')
        for i in range(self.ni):
            print(self.wi[i])
        print()
        print('Output weights:')
        for j in range(self.nh):
            print(self.wo[j])

    def train(self, patterns, iterations=1000, N=0.5, M=0.1):
        # N: learning rate
        # M: momentum factor
        for i in range(iterations):
            error = 0.0
            for p in patterns:
                inputs = p[0]
                targets = p[1]
                self.update(inputs)
                error = error + self.backPropagate(targets, N, M)
            if i % 100 == 0:
                print('error %-.5f' % error)


def demo():
    # Teach network XOR function
    '''
    pat = [
        [[0,0], [0]],
        [[0,1], [1]],
        [[1,0], [1]],
        [[1,1], [0]]
    ]
    '''
    train_samples = []
    test_samples = []
    count = 1
    with open('NNt.out') as f:
        for line in f:
           inner_list = [float(elt.strip()) for elt in line.split(' ')]
           if count % 9 == 0: 
              test_samples.append(inner_list)
           else:
              train_samples.append(inner_list)
           count += 1 

    train_lables = []
    test_lables = []
    count = 1
    with open('Yt.out') as f:
        for line in f:
           inner_list = [float(elt.strip()) for elt in line.split(' ')]
           if count % 9 == 0: 
              test_lables.append(inner_list)
           else:
              train_lables.append(inner_list)
           count += 1 
    #print train_samples[0]
    #print train_lables[0]
    #print test_samples[0]
    #print test_lables[0]
    train_samples_T = numpy.array(train_samples).transpose()
    test_samples_T = numpy.array(test_samples).transpose()

    train_samples_T_T= []
    for row in train_samples_T:
       norm_row = [float(k) /max(row) for k in row]
       train_samples_T_T.append(norm_row)

    test_samples_T_T= []
    for row in test_samples_T:
       norm_row = [float(k) /max(row) for k in row]
       test_samples_T_T.append(norm_row)

    train_mat = {k : [] for k in range(1,261)}
    for row,row1 in zip(train_samples, train_lables):
       c = 1
       for x in row1:
          temp_list = [row, [x]]
          train_mat[c].append(temp_list)
          c += 1

    train_samples = numpy.array(train_samples_T_T).transpose()
    test_samples = numpy.array(test_samples_T_T).transpose()

     
    train_mat = []
    for row,row1 in zip(train_samples, train_lables):
       temp_list = [row, row1]
       train_mat.append(temp_list)
      
    #print train_mat[0] 

    test_mat = []
    for row,row1 in zip(test_samples, test_lables):
       temp_list = [row, row1]
       test_mat.append(temp_list)
    # create a network with two input, two hidden, and one output nodes
    n = NN(100, 100, 260)
    # train it with some patterns
    #n.train(pat)
    n.train(train_mat)
    print "Done training\n"
    # test it
    #n.test(pat)
    result = n.test(test_mat)
    print "=============result============="
    print result
    print result[0]
    result_final =[]
    for row in result:
       temp = [1.0 if x>=0.5 else 0.0 for x in row]
       result_final.append(temp)
    print "=============result digits============="
    print result_final
    print result_final[0]

    '''
    # For precision:
    test_lables = [[1.0, 0.0, 1.0], [1.0, 1.0, 1.0],[0.0,0.0,0.0]]
    result_final = [[0.0, 0.0, 0.0], [1.0, 1.0, 1.0], [1.0, 0.0, 1.0]]
    test_lables_ = numpy.array(test_lables).transpose()
    result_final_ = numpy.array(result_final).transpose()
   
    print test_lables
    print test_lables_
    
    print result_final
    print result_final_
    
    '''
    test_lables_ = numpy.array(test_lables).transpose()
    result_final_ = numpy.array(result_final).transpose()

    print test_lables
    print test_lables_
    
    print result_final
    print result_final_
    precision_ = []
    recall_ = []
    f1_score_ = []
    for row,row1 in zip(test_lables_, result_final_):
       print row, row1
       m1 = 0
       m2 = 0
       m3 = 0
       precision = 0
       recall = 0
       f1_score = 0
       for item,item1 in zip(row,row1):
          if (int(item) == 1):
             m1 += 1
          if (int(item1) == 1):
             m2 += 1
          if (int(item1) == 1)  and (int(item) == int(item1)):
             m3 += 1
       print "m1: %d, m2 : %d , m3 %d" %(m1,m2,m3) 
       #print "========Lable=========="
       #print "hits: %d" %(hits)
       #print "onlyPositives: %d" %(onlyPositives)
       #print "allPositives %d" %(allPositives)
       if m2 > 0:
          precision = float(m3) / m2
          precision_.append(precision)
       if m1 > 0:
          recall = float(m3)/ m1
          recall_.append(recall)
       if (precision > 0) or (recall > 0) :
          f1_score = (2 * precision * recall) / (precision + recall)
          f1_score_.append(f1_score)
    
    print "Precision_: %d" %sum(precision_)
    print precision_
    print "Recall_: %d" %sum(recall_)
    print recall_
    print "f1_score_: %d" %sum(f1_score_)
    print f1_score_
    
    print "Precision: %f" %(float(sum(precision_)) / len(precision_))
    print "Recall: %f" %(float(sum(recall_)) / len(recall_))
    print "f1_score: %f" %(float(sum(f1_score_)) / len(f1_score_))
        
         

if __name__ == '__main__':
    demo()
